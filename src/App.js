import FormHeading from "./Components/formheading";
import Form from "./Components/form";
import "./App.css";

function App() {
  return (
    <div className="App">
      <FormHeading />
      <Form />
    </div>
  );
}

export default App;
