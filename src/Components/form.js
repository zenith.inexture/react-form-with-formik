import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";

//assign initial values
const initialValues = {
  uname: "",
  radiobtn: "",
  email: "",
  pword: "",
};

//onsubmit function
const onSubmit = (values) => {
  console.log(values);
};

//validate function with out yup lai.
const validate = (values) => {
  let errors = {};

  if (!values.uname) {
    errors.uname = "Required";
  }

  if (!values.email) {
    errors.email = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
    errors.email = "Invalid email format";
  }

  if (!values.pword) {
    errors.pword = "Required";
  }

  return errors;
};

//validation with yup object
const validationSchema = Yup.object({
  uname: Yup.string().required("Required"),
  radiobtn: Yup.string().required("Required"),
  email: Yup.string()
    .matches(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, "Invalid email format")
    .required("Required"),
  pword: Yup.string()
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(/^[a-z0-9]+$/i, "Password can only contain letters and numbers")
    .required("Required"),
});

//form function with useformik
function Form() {
  const formik = useFormik({
    initialValues,
    onSubmit,
    // validate,     with regular formik function
    validationSchema, //with yup lib
  });

  // console.log(formik.values);    value of all input field
  // console.log(formik.errors);    error msg for all input field
  // console.log(formik.touched);   ture of false for all visited field

  //return jsx
  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <label>
          Username:
          <input
            type="text"
            id="uname"
            name="uname"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.uname}
          />
        </label>
        {formik.errors.uname && formik.touched.uname ? (
          <p>{formik.errors.uname}</p>
        ) : null}
        <br></br>
        Gender:
        <label>
          <input
            type="radio"
            name="radiobtn"
            value="male"
            onClick={formik.handleChange}
          />
          Male
        </label>
        <label>
          <input
            type="radio"
            name="radiobtn"
            value="female"
            onClick={formik.handleChange}
          />
          Female
        </label>
        {formik.errors.radiobtn && formik.touched.radiobtn ? (
          <p>{formik.errors.radiobtn}</p>
        ) : null}
        <br></br>
        <label>
          Email:
          <input
            type="email"
            id="email"
            name="email"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.email}
          />
        </label>
        {formik.errors.email && formik.touched.email ? (
          <p>{formik.errors.email}</p>
        ) : null}
        <br></br>
        <label>
          Password:
          <input
            type="password"
            id="pword"
            name="pword"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.pword}
          />
        </label>
        {formik.errors.pword && formik.touched.pword ? (
          <p>{formik.errors.pword}</p>
        ) : null}
        <br></br>
        <button type="submit">Submit</button>
        <button type="reset" onClick={formik.resetForm}>
          Reset
        </button>
      </form>
    </>
  );
}
export default Form;
